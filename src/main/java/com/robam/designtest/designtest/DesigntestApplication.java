package com.robam.designtest.designtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication(exclude = {
        DataSourceAutoConfiguration.class //取消默认的datasource自动配置，因为我们yml配置结构变了
})
@ComponentScan(basePackages = {"com.robam"}) //扫描所有组件java类配置
@EnableAsync
public class DesigntestApplication {

    public static void main(String[] args) {
        SpringApplication.run(DesigntestApplication.class, args);
    }

}

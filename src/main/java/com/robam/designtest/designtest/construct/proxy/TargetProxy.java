/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.construct.proxy;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/26 8:06
 * @Version 1.0
 */
@Slf4j
public class TargetProxy extends Target {

    public Target target;

    public TargetProxy(Target target) {
        this.target = target;
    }

    @Override
    public void print() {
        log.info("before");
        target.print();
        log.info("after");
    }
}

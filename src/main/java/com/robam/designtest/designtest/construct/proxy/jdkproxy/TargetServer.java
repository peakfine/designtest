/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.construct.proxy.jdkproxy;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/26 8:53
 * @Version 1.0
 */
@Slf4j
public class TargetServer implements Target {
    @Override
    public void print() {
        log.info("do something");
    }
}

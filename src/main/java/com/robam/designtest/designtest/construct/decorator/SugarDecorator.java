/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.construct.decorator;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/25 17:15
 * @Version 1.0
 */
@Slf4j
public class SugarDecorator extends FeedingDecorator {
    public SugarDecorator(Noodle noodle) {
        super(noodle);
    }

    @Override
    public void print() {
        super.noodle.print();
        log.info("给面加糖");
    }
}

/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.construct.proxy;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/26 8:05
 * @Version 1.0
 */
@Slf4j
public class TargetServer extends Target {
    public TargetServer() {
        log.info("create instance");
    }


    @Override
    public void print() {
        log.info("do something");
    }
}

/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.construct.bridge;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/25 16:35
 * @Version 1.0
 */
@Slf4j
public class CommonMessage extends AbstractMessage {

    public CommonMessage(MessageImplementor messageImplementor) {
        super(messageImplementor);
    }

    @Override
    public void send() {
        super.messageImplementor.send();
        log.info("发送普通消息");
    }
}

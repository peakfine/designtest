/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.construct.proxy;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/26 8:04
 * @Version 1.0
 */
public abstract class Target {

    public abstract void print();
}

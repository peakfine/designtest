/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.construct.decorator;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/25 17:07
 * @Version 1.0
 */
public abstract class Noodle {
    public abstract void print();
}

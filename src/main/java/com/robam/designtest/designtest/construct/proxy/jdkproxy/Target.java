/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.construct.proxy.jdkproxy;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/26 8:53
 * @Version 1.0
 */
public interface Target {
    void print();
}

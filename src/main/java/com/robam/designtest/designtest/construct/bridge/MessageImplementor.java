/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.construct.bridge;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/25 16:35
 * @Version 1.0
 */
public interface MessageImplementor {
    /**
     * 使用XX发送消息
     */
    void send();
}

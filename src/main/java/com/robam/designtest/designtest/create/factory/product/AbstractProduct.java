/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.create.factory.product;

/**
 * @Description 产品的抽象父类 车
 * @Author 薛铁琪
 * @CreateTime 2021/3/25 11:00
 * @Version 1.0
 */
public abstract class AbstractProduct {
    /**
     * 自我介绍
     */
    public abstract void print();
}

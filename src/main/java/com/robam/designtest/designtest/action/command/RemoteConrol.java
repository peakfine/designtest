/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.action.command;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/27 10:28
 * @Version 1.0
 */
public class RemoteConrol {

    Command command;

    public RemoteConrol(Command command) {
        this.command = command;
    }

    public void buttonWasPressed() {
        command.execute();
    }

}

/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.action.responsibility;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/26 15:05
 * @Version 1.0
 */
@Slf4j
public class InfoLogger extends Logger {


    public InfoLogger(Logger nextLogger) {
        super(Logger.INFO, nextLogger);
    }

    @Override
    public void write(String message) {
        log.info("INFO log:{}", message);
    }
}

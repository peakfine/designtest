/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.action.observer;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/26 11:22
 * @Version 1.0
 */
public interface Observer {

    /**
     * 接收变动通知
     */
    void update();
}

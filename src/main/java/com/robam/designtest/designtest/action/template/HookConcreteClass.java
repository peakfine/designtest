/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.action.template;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/27 16:09
 * @Version 1.0
 */
@Slf4j
public class HookConcreteClass extends HookAbstractClass {
    @Override
    public void abstractMethod1() {
        log.info("抽象方法1的实现被调用...");
    }

    @Override
    public void abstractMethod2() {
        log.info("抽象方法2的实现被调用...");
    }

    @Override
    public void HookMethod1() {
        log.info("钩子方法1被重写...");
    }

    @Override
    public boolean HookMethod2() {
        return false;
    }
}
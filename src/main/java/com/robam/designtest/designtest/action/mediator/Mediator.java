/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.action.mediator;

/**
 * @Description 中介者抽象类
 * @Author 薛铁琪
 * @CreateTime 2021/3/27 10:49
 * @Version 1.0
 */
public abstract class Mediator {

    /**
     * 注册同事
     */
    public abstract void register(Colleague colleague);

    /**
     * 通知中介者，发送消息
     */
    public abstract void relay(Colleague colleague);
}

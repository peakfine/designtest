/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.action.state;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/27 12:49
 * @Version 1.0
 */
@Slf4j
public class ThreadStatusB extends ThreadState {

    public ThreadStatusB() {
        this.state = "B";
    }

    @Override
    public void handle(ThreadContext threadContext, ThreadState next) {
        log.info("当前状态{}", this.state);
        log.info("do something by {}", this.state);
        threadContext.setState(next);
        log.info("置为状态{}", next.state);
    }
}

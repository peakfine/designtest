/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.action.observer.jdkobserver;

import lombok.extern.slf4j.Slf4j;

import java.util.Observable;
import java.util.Observer;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/26 13:03
 * @Version 1.0
 */
@Slf4j
public class RealObserver implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        log.info("接收到通知，处理了些事情");
    }
}

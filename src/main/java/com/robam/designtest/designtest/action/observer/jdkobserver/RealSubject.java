/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.action.observer.jdkobserver;

import lombok.extern.slf4j.Slf4j;

import java.util.Observable;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/26 13:00
 * @Version 1.0
 */
@Slf4j
public class RealSubject extends Observable {

    public void print() {
        log.info("do something");
        setChanged();
        notifyObservers();
    }
}

/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.action.mediator;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/27 10:53
 * @Version 1.0
 */
@Slf4j
public class ConcreteColleague2 extends Colleague {
    @Override
    public void receive() {
        log.info("colleague2 receive message.");
    }

    @Override
    public void send() {
        log.info("colleague2 send message.");
        //请中介者转发
        this.mediator.relay(this);
    }
}

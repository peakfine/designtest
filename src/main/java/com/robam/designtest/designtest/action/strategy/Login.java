/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.action.strategy;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/26 10:13
 * @Version 1.0
 */
public interface Login {
    /**
     * 登陆接口
     */
    void print();
}

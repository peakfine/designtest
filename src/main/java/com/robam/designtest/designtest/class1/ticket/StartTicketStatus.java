/**
 * COPYRIGHT HangZhou 99Cloud Technology Company Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.class1.ticket;

import com.robam.designtest.designtest.class1.Request;
import com.robam.designtest.designtest.class1.alarm.Alarm;
import com.robam.designtest.designtest.class1.alarm.listen.AlarmNotifyPublisher;
import com.robam.designtest.designtest.class1.device.Device;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/10/12 16:45
 * @Version 1.0
 */
public class StartTicketStatus extends TicketStatus {


    public StartTicketStatus(Ticket ticket) {
        super(ticket);
    }

    @Override
    public TicketStatus dealingTicket(Alarm alarm, Device device) {
        // todo 更新工单状态 -->处理中
        Ticket ticket = getTicket();
        ticket.setStatus("处理中");
        // todo 更新告警状态 --> 处理中
        AlarmNotifyPublisher alarmNotifyPublisher = new AlarmNotifyPublisher();
        alarmNotifyPublisher.publishNotify(Request.builder().id(1L).request("dealing").alarm(alarm).device(device).build());
        return new DealingTicketStatus(getTicket());
    }
}

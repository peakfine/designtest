package com.robam.designtest.designtest.class1.alarm.filter;

import com.robam.designtest.designtest.class1.Request;
import com.robam.designtest.designtest.class1.Response;

public interface AlarmTransferFilter {


    public void doFilter(Request req, Response response, AlarmTransferFilterChain chain);
}
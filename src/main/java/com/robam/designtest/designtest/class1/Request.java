package com.robam.designtest.designtest.class1;

import com.robam.designtest.designtest.class1.alarm.Alarm;
import com.robam.designtest.designtest.class1.device.Device;
import com.robam.designtest.designtest.class1.ticket.Ticket;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Request {
    private String request;
    private Long id;
    private Alarm alarm;
    private Device device;
    private Ticket ticket;
}
/**
 * COPYRIGHT HangZhou 99Cloud Technology Company Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.class1.ticket;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/10/12 16:45
 * @Version 1.0
 */
public class FinishTicketStatus extends TicketStatus {
    public FinishTicketStatus(Ticket ticket) {
        super(ticket);
    }
}

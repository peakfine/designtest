/**
 * COPYRIGHT HangZhou 99Cloud Technology Company Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.class1.ticket;

import com.robam.designtest.designtest.class1.alarm.Alarm;
import com.robam.designtest.designtest.class1.device.Device;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/10/12 13:50
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Ticket {

    private Long id;
    private String desc;
    private String status;

    private TicketStatus ticketStatus;


    /**
     * 数据库拿到对象后 记得初始化ticketStatus
     */
    public void initTicketStatus() {
        // todo 根据数据库查询出来的status确定当前的ticketStatus
        this.ticketStatus = new StartTicketStatus(this);
    }


    public void newTicket() {
        // todo 写入数据库
        this.ticketStatus = new StartTicketStatus(this);
    }

    public void dealingTicket(Alarm alarm, Device device) {
        this.ticketStatus = this.ticketStatus.dealingTicket(alarm, device);
    }

    public void cancel(Alarm alarm, Device device) {
        this.ticketStatus = this.ticketStatus.cancel(alarm, device);
    }

    public void finishTicket(Alarm alarm, Device device) {
        this.ticketStatus = this.ticketStatus.finishTicket(alarm, device);
    }

    public void giveUpTicket(Alarm alarm, Device device) {
        this.ticketStatus = this.ticketStatus.giveUpTicket(alarm, device);
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", desc='" + desc + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}

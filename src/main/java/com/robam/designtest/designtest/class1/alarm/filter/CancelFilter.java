package com.robam.designtest.designtest.class1.alarm.filter;

import com.robam.designtest.designtest.class1.Request;
import com.robam.designtest.designtest.class1.Response;
import com.robam.designtest.designtest.class1.alarm.Alarm;

public class CancelFilter implements AlarmTransferFilter {

    @Override
    public void doFilter(Request req, Response response, AlarmTransferFilterChain chain) {
        if ("cancel".equals(req.getRequest())) {
            //todo 拿到对应的alarm do cancel
            Alarm alarm = req.getAlarm();
            alarm.cancel(req.getDevice());
        }
        chain.doFilter(req, response, chain);
    }
}
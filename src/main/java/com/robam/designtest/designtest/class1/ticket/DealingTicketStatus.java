/**
 * COPYRIGHT HangZhou 99Cloud Technology Company Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.class1.ticket;

import com.robam.designtest.designtest.class1.Request;
import com.robam.designtest.designtest.class1.alarm.Alarm;
import com.robam.designtest.designtest.class1.alarm.listen.AlarmNotifyPublisher;
import com.robam.designtest.designtest.class1.device.Device;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/10/12 16:45
 * @Version 1.0
 */
public class DealingTicketStatus extends TicketStatus {
    public DealingTicketStatus(Ticket ticket) {
        super(ticket);
    }

    @Override
    public TicketStatus cancel(Alarm alarm, Device device) {
        // todo 转单 更新为 -->开始状态
        Ticket ticket = getTicket();
        ticket.setStatus("返回开始状态");
        // todo 告警 更新为 -->开始状态
        AlarmNotifyPublisher alarmNotifyPublisher = new AlarmNotifyPublisher();
        alarmNotifyPublisher.publishNotify(Request.builder().id(1L).request("cancel").alarm(alarm).device(device).build());
        return new StartTicketStatus(getTicket());
    }

    @Override
    public TicketStatus finishTicket(Alarm alarm, Device device) {
        // todo   工单更新为 -->完成
        Ticket ticket = getTicket();
        ticket.setStatus("处理完成");
        // todo   告警更新为 -->完成
        AlarmNotifyPublisher alarmNotifyPublisher = new AlarmNotifyPublisher();
        alarmNotifyPublisher.publishNotify(Request.builder().id(1L).request("finish").alarm(alarm).device(device).build());
        return new FinishTicketStatus(getTicket());
    }

    @Override
    public TicketStatus giveUpTicket(Alarm alarm, Device device) {
        // todo   工单更新为 -->无法完成
        Ticket ticket = getTicket();
        ticket.setStatus("放弃处理");
        // todo   告警更新为 -->无法完成
        AlarmNotifyPublisher alarmNotifyPublisher = new AlarmNotifyPublisher();
        alarmNotifyPublisher.publishNotify(Request.builder().id(1L).request("giveUp").alarm(alarm).device(device).build());
        return new GiveUpTicketStatus(getTicket());
    }
}

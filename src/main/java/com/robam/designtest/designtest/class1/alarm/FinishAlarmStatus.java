/**
 * COPYRIGHT HangZhou 99Cloud Technology Company Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.class1.alarm;

/**
 * @Description 处理状态能做的动作 以及返回的下一个状态
 * @Author 薛铁琪
 * @CreateTime 2021/10/12 15:11
 * @Version 1.0
 */
public class FinishAlarmStatus extends AlarmStatus {

    public FinishAlarmStatus(Alarm alarm) {
        super(alarm);
    }
}

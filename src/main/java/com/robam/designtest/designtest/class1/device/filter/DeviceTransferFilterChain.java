/**
 * COPYRIGHT HangZhou 99Cloud Technology Company Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.class1.device.filter;

import com.robam.designtest.designtest.class1.Request;
import com.robam.designtest.designtest.class1.Response;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/10/13 14:30
 * @Version 1.0
 */
public class DeviceTransferFilterChain implements DeviceTransferFilter {

    private List<DeviceTransferFilter> filters = new ArrayList<DeviceTransferFilter>();
    int index = 0;

    public DeviceTransferFilterChain addFilter(DeviceTransferFilter filter) {
        this.filters.add(filter);
        return this;
    }


    @Override
    public void doFilter(Request req, Response res, DeviceTransferFilterChain chain) {
        if (index == filters.size()) {
            return;
        }
        DeviceTransferFilter filter = filters.get(index);
        index++;
        filter.doFilter(req, res, chain);
    }
}
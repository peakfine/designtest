package com.robam.designtest.designtest.class1.alarm.filter;

import com.robam.designtest.designtest.class1.Request;
import com.robam.designtest.designtest.class1.Response;
import com.robam.designtest.designtest.class1.alarm.Alarm;

public class GiveUpFilter implements AlarmTransferFilter {

    @Override
    public void doFilter(Request req, Response response, AlarmTransferFilterChain chain) {
        if ("giveUp".equals(req.getRequest())) {
            //todo 拿到对应的alarm do giveUp
            Alarm alarm = req.getAlarm();
            alarm.giveUpAlarm(req.getDevice());
        }
        chain.doFilter(req, response, chain);
    }
}
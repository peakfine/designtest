package com.robam.designtest.designtest.class1.device.listen;

import com.robam.designtest.designtest.class1.Request;
import com.robam.designtest.designtest.class1.Response;
import com.robam.designtest.designtest.class1.device.filter.*;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/5/14 14:11
 * @Version 1.0
 */
@Component
public class DeviceNotifyEventListener {


    @EventListener
    @Async
    public void listener(DeviceNotifyEvent event) {
        Request request = (Request) event.getData();
        DeviceTransferFilterChain deviceTransferFilterChain = new DeviceTransferFilterChain();
        AbnormalFilter abnormalFilter = new AbnormalFilter();
        DealingFilter dealingFilter = new DealingFilter();
        CancelFilter cancelFilter = new CancelFilter();
        FinishFilter finishFilter = new FinishFilter();
        GiveUpFilter giveUpFilter = new GiveUpFilter();
        deviceTransferFilterChain.addFilter(abnormalFilter).addFilter(dealingFilter).addFilter(cancelFilter).addFilter(finishFilter).addFilter(giveUpFilter);
        deviceTransferFilterChain.doFilter(request, new Response(), deviceTransferFilterChain);
    }
}

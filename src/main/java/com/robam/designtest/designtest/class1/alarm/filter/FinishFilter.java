package com.robam.designtest.designtest.class1.alarm.filter;

import com.robam.designtest.designtest.class1.Request;
import com.robam.designtest.designtest.class1.Response;
import com.robam.designtest.designtest.class1.alarm.Alarm;

public class FinishFilter implements AlarmTransferFilter {

    @Override
    public void doFilter(Request req, Response response, AlarmTransferFilterChain chain) {
        if ("finish".equals(req.getRequest())) {
            //todo 拿到对应的alarm do finish
            Alarm alarm = req.getAlarm();
            alarm.finishAlarm(req.getDevice());
        }
        chain.doFilter(req, response, chain);
    }
}
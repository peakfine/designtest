/**
 * COPYRIGHT HangZhou 99Cloud Technology Company Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.class1.device;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/10/12 16:22
 * @Version 1.0
 */
public class NormalDeviceStatus extends DeviceStatus {

    public NormalDeviceStatus(Device device) {
        super(device);
    }

    @Override
    public DeviceStatus abnormalDevice() {
        // todo 设备状态 --> 异常
        Device device = getDevice();
        device.setStatus("异常状态");
        return new AbnormalDeviceStatus(getDevice());
    }


}

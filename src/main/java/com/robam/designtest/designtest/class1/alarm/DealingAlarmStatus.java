/**
 * COPYRIGHT HangZhou 99Cloud Technology Company Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.class1.alarm;

import com.robam.designtest.designtest.class1.Request;
import com.robam.designtest.designtest.class1.device.Device;
import com.robam.designtest.designtest.class1.device.listen.DeviceNotifyPublisher;

/**
 * @Description 处理状态能做的动作 以及返回的下一个状态
 * @Author 薛铁琪
 * @CreateTime 2021/10/12 15:11
 * @Version 1.0
 */
public class DealingAlarmStatus extends AlarmStatus {


    public DealingAlarmStatus(Alarm alarm) {
        super(alarm);
    }

    @Override
    public AlarmStatus finishAlarm(Device device) {
        // todo 更新警告状态 -->处理完成
        Alarm alarm = getAlarm();
        alarm.setStatus("处理完成");
        // todo 设备状态 --> 正常
        DeviceNotifyPublisher deviceNotifyPublisher = new DeviceNotifyPublisher();
        deviceNotifyPublisher.publishNotify(Request.builder().id(1L).request("finish").device(device).build());
        return new FinishAlarmStatus(getAlarm());
    }

    @Override
    public AlarmStatus giveUpAlarm(Device device) {
        // todo 更新警告状态 -->无法处理
        Alarm alarm = getAlarm();
        alarm.setStatus("无法处理");
        // todo 设备状态 --> 报废
        DeviceNotifyPublisher deviceNotifyPublisher = new DeviceNotifyPublisher();
        deviceNotifyPublisher.publishNotify(Request.builder().id(1L).request("giveUp").device(device).build());
        return new GiveUpAlarmStatus(getAlarm());
    }

    @Override
    public AlarmStatus cancelAlarm(Device device) {
        // todo 更新警告状态 -->告警中
        // todo 设备状态 --> 异常
        Alarm alarm = getAlarm();
        alarm.setStatus("状态恢复告警中");
        DeviceNotifyPublisher deviceNotifyPublisher = new DeviceNotifyPublisher();
        deviceNotifyPublisher.publishNotify(Request.builder().id(1L).request("cancel").device(device).build());
        return new StartAlarmStatus(getAlarm());
    }
}
